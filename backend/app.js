const express = require("express");
const { sequelize, User } = require("./models"); // import models
const cors = require("cors");

const app = express(); // create a new express app
app.use(express.json());

var corsOptions = {
  origin: ["https://crud-app-gold.vercel.app", "https://crud-smoke.vercel.app"],
};

app.use(cors(corsOptions));

app.listen({ port: 5005 }, async () => {
  await sequelize.sync();
  await sequelize.authenticate();
  console.log("running");
});

app.post("/create", async (req, res) => {
  const { name, phone, email } = req.body;
  try {
    const user = await User.create({ name, phone, email });
    return res.json(user);
  } catch (err) {
    return res.status(500).json(err);
  }
});

app.get("/", async (req, res) => {
  try {
    const users = await User.findAll();
    return res.json(users);
  } catch (err) {
    return res.status(500).json({ err: "An error occured" });
  }
});

app.get("/userbio/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const user = await User.findOne({
      where: { id },
    });

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    return res.json(user);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "An error occurred" });
  }
});

app.put("/editusers/:id", async (req, res) => {
  const id = req.params.id;
  const { name, email, phone } = req.body;
  try {
    const user = await User.findOne({
      where: { id },
    });
    user.name = name;
    user.email = email;
    user.phone = phone;

    await user.save();
    return res.json(user);
  } catch (err) {
    return res.status(500).json({ err: "An error occured" });
  }
});

app.delete("/deleteusers/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const user = await User.findOne({
      where: { id },
    });
    await user.destroy();
    return res.json({ message: "User Deleted" });
  } catch (err) {
    return res.status(500).json({ err: "An error occured" });
  }
});
